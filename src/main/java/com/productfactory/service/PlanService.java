package com.productfactory.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.productfactory.domain.Plan;
import com.productfactory.repository.PlanRepository;

@Service
public class PlanService {

	private PlanRepository planRepository;

	public PlanService(PlanRepository planRepository) {
		this.planRepository = planRepository;
	}

	public Iterable<Plan> list() {
		return planRepository.findAll();
	}

	public Iterable<Plan> save(List<Plan> plans) {
		return planRepository.saveAll(plans);
	}

	public Plan save(Plan plan) {
		return planRepository.save(plan);
	}
}
