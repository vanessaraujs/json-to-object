package com.productfactory.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.productfactory.domain.Plan;

@Repository
public interface PlanRepository extends CrudRepository<Plan, Long> {

}
