package com.productfactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.FilenameUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.productfactory.domain.Plan;
import com.productfactory.service.PlanService;

@SpringBootApplication
public class ProductfactoryApplication {

	public static void main(String[] args) throws IOException, InterruptedException {
		SpringApplication.run(ProductfactoryApplication.class, args);

		WatchService watchService = FileSystems.getDefault().newWatchService();
		Path diretorio = Paths.get("/home/vanessa/Documents/json-files");
		diretorio.register(watchService, StandardWatchEventKinds.ENTRY_CREATE);
		while (true) {
			WatchKey watchKey = watchService.take();
			Optional<WatchEvent<?>> watchEvent = watchKey.pollEvents().stream().findFirst();
			if (watchEvent.isPresent()) {
				if (watchEvent.get().kind() == StandardWatchEventKinds.OVERFLOW) {
					continue;
				}

				Path path = (Path) watchEvent.get().context();

				if (!FilenameUtils.getExtension(path.toString()).equalsIgnoreCase("json")) {
					System.out.println("Não é um arquivo de extensão válida.");
				}

//	      >>>>>> Recupera o nome do arquivo >> eventPath.getName(0);
//	       eventPath.getName(0).endsWith("json-commarea");
			}
			boolean valid = watchKey.reset();
			if (!valid) {
				break;
			}
		}
		watchService.close();
	}

	// TODO:
	// Inserir esse código dentro do IF da linha 45, para que possa converter de
	// json para objeto após validar extensao do arquivo
	@Bean
	CommandLineRunner runner(PlanService planService) {
		return args -> {
			ObjectMapper mapper = new ObjectMapper();
			TypeReference<List<Plan>> typeReference = new TypeReference<List<Plan>>() {
			};
			InputStream inputStream = TypeReference.class.getResourceAsStream("/json-files/plan.json");
			try {
				List<Plan> plans = mapper.readValue(inputStream, typeReference);
				planService.save(plans);
				System.out.println("Plans Saved!");
			} catch (IOException e) {
				System.out.println("Unable to save plans: " + e.getMessage());
			}
		};
	}

}
