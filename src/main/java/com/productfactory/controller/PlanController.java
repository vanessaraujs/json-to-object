package com.productfactory.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.productfactory.domain.Plan;
import com.productfactory.service.PlanService;

@RestController
@RequestMapping("/plans")
public class PlanController {

	private PlanService planService;

	public PlanController(PlanService planService) {
		this.planService = planService;
	}

	@GetMapping("/list")
	public Iterable<Plan> list() {
		return planService.list();
	}

}
